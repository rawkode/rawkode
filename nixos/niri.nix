{
  inputs,
  pkgs,
  ...
}:
{
  imports = [
    inputs.niri.nixosModules.niri
  ];

  environment.systemPackages = with pkgs; [
    fuzzel
    xwayland
    xwayland-satellite
  ];

  programs.niri.enable = true;
}
