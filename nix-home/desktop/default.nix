{
  imports = [
    ./ghostty.nix
    ./gnome.nix
		./niri.nix
    ./wezterm/default.nix
    ./web.nix
  ];
}
