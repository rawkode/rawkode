{
  imports = [
    ./atuin.nix
    ./gh.nix
		./nushell.nix
    ./shell.nix
    ./zellij.nix
    ./zoxide.nix
  ];
}
